import { Host, Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiTodolist {
  private url: string = 'https://aqueous-plateau-40844.herokuapp.com/';

  constructor(private http: HttpClient) {}

  usersCall(email: string, password: string): Observable<any> {
    var toJson = {
      username: email,
      password: password,
    };

    const headers = { 'content-type': 'application/json' };
    const body = JSON.stringify(toJson);

    return this.http.post(this.url + 'api/login', body, {
      headers: headers,
    });
  }

  signUpUser(email: string, password: string): Observable<any> {
    var toJson = {
      username: email,
      roles: ['user'],
      password: password,
    };

    const body = JSON.stringify(toJson);
    const headers: any = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('host', '127.0.0.1');

    return this.http.post(this.url + 'api/users', body, {
      headers: headers,
    });
  }

  getIdCurrentUser(email: string) {
    var listOfUser: any = [];

    this.http.get(this.url + 'api/users.json').subscribe((data) => {
      listOfUser = data;

      listOfUser.forEach((person: any) => {
        if (person['email'] == email) {
          return person['id'];
        }
      });
    });
  }

  todolistCall() {
    return this.http.get(this.url + 'api/todolists.json');
  }

  restofPlace(idConcert: string) {
    return this.http.get(this.url + 'api/orders?idConcert=' + idConcert);
  }
}
