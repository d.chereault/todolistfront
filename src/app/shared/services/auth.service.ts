import { Injectable, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.models';
import { ApiTodolist } from './apiTodolist.service';

import { CookieService } from 'ngx-cookie';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public loggedUser!: string;
  public isloggedIn!: Boolean;
  constructor(
    private api: ApiTodolist,
    private cookieConnexion: CookieService
  ) {}

  logout() {
    this.isloggedIn = false;
    this.cookieConnexion.remove('loggedUser');
    this.cookieConnexion.remove('isOP');
    this.cookieConnexion.put('isloggedIn', String(this.isloggedIn), {
      //httpOnly: true,
    });
  }

  async SignIn(user: User): Promise<boolean> {
    let validUser: boolean = false;
    let promise = new Promise((resolve, reject) => {
      this.api
        .usersCall(user.email, user.password)
        .toPromise()
        .then((res) => {
          this.isloggedIn = true;
          this.cookieConnexion.put('loggedUser', String(res.token), {
            //httpOnly: true,
          });
          this.cookieConnexion.put('isloggedIn', String(this.isloggedIn), {
            //httpOnly: true,
            expires: new Date(new Date().getTime() + 5400000),
          });
        });
      validUser = true;
    });

    return validUser;
  }
}
