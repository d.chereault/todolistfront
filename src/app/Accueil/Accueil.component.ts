import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalDirective } from 'angular-bootstrap-md';
import { Concert } from '../models/concert.models';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiTodolist } from '../shared/services/apiTodolist.service';
import { ColumnMode } from '@swimlane/ngx-datatable';

import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-Accueil',
  templateUrl: './Accueil.component.html',
  styleUrls: ['./Accueil.component.scss'],
})
export class AccueilComponent {
  @ViewChild(ModalDirective) modal!: ModalDirective;

  data: any = [];
  todolists: any = [];
  tempTodolist: any = [];
  rows!: any[];
  columns: any = [];

  constructor(
    private api: ApiTodolist,
    private httpClient: HttpClient,
    private router: Router,
    public cookiePanier: CookieService
  ) {}

  async ngOnInit() {
    this.AsyncData().then(
      () => {
        this.todolists.forEach((todolist: any, indexTodo: number) => {
          this.todolists[indexTodo].taches.forEach(
            (tache: any, indexTache: number) => {
              this.todolists[indexTodo].taches[indexTache]['User'] =
                this.todolists[indexTodo].taches[indexTache].idUser.username;
              this.todolists[indexTodo].taches[indexTache]['Status'] =
                this.todolists[indexTodo].taches[indexTache].idStatus.title;
            }
          );
          console.warn(this.todolists);
        });
      },
      () => console.warn('AsyncData Errored!')
    );
  }

  AsyncData(): Promise<any> {
    let promise = new Promise<void>(async (resolve, reject) => {
      this.api
        .todolistCall()
        .toPromise()
        .then((data: any) => {
          this.todolists = data;
          resolve();
        });
    });

    return promise;
  }

  click(id: any) {
    console.warn(id);
  }
}
