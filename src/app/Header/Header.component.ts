import { ViewChild } from '@angular/core';
import { Component, Inject, OnInit } from '@angular/core';
import { ModalContainerComponent, ModalDirective } from 'angular-bootstrap-md';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.models';
import { AuthService } from '../shared/services/auth.service';

import { CookieService } from 'ngx-cookie';
import { Router } from '@angular/router';

import { ApiTodolist } from '../shared/services/apiTodolist.service';

@Component({
  selector: 'app-Header',
  templateUrl: './Header.component.html',
  styleUrls: ['./Header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @ViewChild('login', { static: true })
  login!: ModalContainerComponent;
  user = new User();

  @ViewChild('register', { static: true })
  register!: ModalContainerComponent;

  validatingForm: any;
  registerForm: any;
  isAuth: boolean = false;
  error: any;

  constructor(
    public authService: AuthService,
    public cookieConnexion: CookieService,
    public router: Router,
    private api: ApiTodolist
  ) {
    this.registerForm = new FormGroup({
      registerFormModalEmail: new FormControl('', Validators.email),
      registerFormModalPassword: new FormControl('', Validators.required),
      registerFormModalPassword2: new FormControl('', Validators.required),
    });

    this.validatingForm = new FormGroup({
      loginFormModalEmail: new FormControl('', Validators.email),
      loginFormModalPassword: new FormControl('', Validators.required),
    });
  }
  ngOnInit(): void {}

  get loginFormModalEmail() {
    return this.validatingForm.get('loginFormModalEmail');
  }

  get loginFormModalPassword() {
    return this.validatingForm.get('loginFormModalPassword');
  }

  get registerFormModalEmail() {
    return this.registerForm.get('registerFormModalEmail');
  }

  get registerFormModalPassword() {
    return this.registerForm.get('registerFormModalPassword');
  }

  get registerFormModalPassword2() {
    return this.registerForm.get('registerFormModalPassword2');
  }

  onInscription() {
    const email: string = this.registerForm.get('registerFormModalEmail').value;
    const password: string = this.registerForm.get(
      'registerFormModalPassword'
    ).value;
    const password2: string = this.registerForm.get(
      'registerFormModalPassword2'
    ).value;

    if (password === password2) {
      this.api.signUpUser(email, password).subscribe((data: any) => {});

      this.register.hide();
    }
  }

  async onConnexion() {
    const email: string = this.validatingForm.get('loginFormModalEmail').value;
    const password: string = this.validatingForm.get(
      'loginFormModalPassword'
    ).value;

    let isValidUser: Promise<boolean> = this.authService.SignIn(this.user);

    if (await isValidUser) {
      this.login.hide();
    } else console.warn('error');
  }

  SignOut() {
    this.registerForm = new FormGroup({
      registerFormModalEmail: new FormControl('', Validators.email),
      registerFormModalPassword: new FormControl('', Validators.required),
      registerFormModalPassword2: new FormControl('', Validators.required),
    });

    this.validatingForm = new FormGroup({
      loginFormModalEmail: new FormControl('', Validators.email),
      loginFormModalPassword: new FormControl('', Validators.required),
    });

    this.authService.logout();
    this.router.navigate(['']);
  }
}
