import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AccueilComponent } from './Accueil/Accueil.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './Header/Header.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ColumnMode } from '@swimlane/ngx-datatable';

//Service
import { ApiTodolist } from './shared/services/apiTodolist.service';

//Angular to PHP
import { HttpClientModule } from '@angular/common/http';

//Cookie
import { CookieModule } from 'ngx-cookie';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    FooterComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CookieModule.forRoot(),
    NgxDatatableModule,
  ],
  providers: [ApiTodolist],
  bootstrap: [AppComponent],
})
export class AppModule {}
